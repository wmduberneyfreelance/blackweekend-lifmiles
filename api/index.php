<?php
	// header('Access-Control-Allow-Origin: *');  
	// header('Access-Control-Allow-Headers: Content-Type, Authorization, x-xsrf-token');

    // error_reporting(E_ALL);
    // ini_set('display_errors', 1);

    
require __DIR__ . '/vendor/autoload.php';

// Consumer key
// ck_6daf45a2b13cdbf99190d4dd88b7cb6e7453b9dc


// Consumer secret
// cs_27c7e8db155211e72c09857f2fab283aa5291195

use Automattic\WooCommerce\Client;

$woocommerce = new Client(
    'http://makedigital.com.co/clientes/track/lifemiles/administrator/', 
    'ck_57df7f1634203f7ea3d80ac5fdfd8235fc76ac88', 
    'cs_452918a70b98b9415e0141c0de828c70c9f6df4b',
    [
        'wp_api' => true,
        'version' => 'wc/v1',
    ]
);

// $woocommerce = new Client(
//     'http://blackweekendlm.com/administrator/', 
//     'ck_eb08baa3b0bb40db5b6ddff703c4be5c93eacff9', 
//     'cs_628882375b448ca4b731f10f29f6cd4ca570aa31',
//     [
//         'wp_api' => true,
//         'version' => 'wc/v1',
//     ]
// );


// $cate = $woocommerce->get('products/categories');
// echo "<pre>";
// print_r($cate);
// echo "</pre>";

// Colombia = 15
// Salvador = 16

$filter = "";
if (isset( $_GET['c'] )){
  $filter = $_GET['c'];
}
$country = "Colombia";

$idCategory = getCountryCategory($filter);
if ($idCategory == 16){
  $country = "Salvador";
}
$results = $woocommerce->get('products', [ "per_page" => 50, "category" => $idCategory ]);
// $results = $woocommerce->get('products/categories');

// echo "<pre>";
// print_r($results);
// echo "</pre>";


header('Content-Type: application/json');
$datos = array("products"=>$results, "pais"=>$country);
print_r(json_encode($datos));


function getCountryCategory($filter){
  $codeCat = 15;

  $ip = get_client_ip();
  
  if ($ip != ""){
    if ($filter == "S"){
      $ip = "201.247.218.1";
    }

    // $ip = "201.247.218.1";

    // echo "IP: " . $ip . "<br><br>";
    
    $str = file_get_contents('http://ip-api.com/json/' . $ip);
    $json = json_decode($str, true);

    if ($json['countryCode'] == "SV"){
      $codeCat = 16;
    }
    
    // echo $json['countryCode'];
    // echo "<br><br>";
    // echo '<pre>' . print_r($json, true) . '</pre>';
  }

  return $codeCat;
}

function get_client_ip() {
  $ipaddress = '';
  if (getenv('HTTP_CLIENT_IP'))
      $ipaddress = getenv('HTTP_CLIENT_IP');
  else if(getenv('HTTP_X_FORWARDED_FOR'))
      $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
  else if(getenv('HTTP_X_FORWARDED'))
      $ipaddress = getenv('HTTP_X_FORWARDED');
  else if(getenv('HTTP_FORWARDED_FOR'))
      $ipaddress = getenv('HTTP_FORWARDED_FOR');
  else if(getenv('HTTP_FORWARDED'))
     $ipaddress = getenv('HTTP_FORWARDED');
  else if(getenv('REMOTE_ADDR'))
      $ipaddress = getenv('REMOTE_ADDR');
  else
      $ipaddress = 'UNKNOWN';
  return $ipaddress;
}

